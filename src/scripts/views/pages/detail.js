/* eslint-disable no-invalid-this */
/* eslint-disable max-len */
/* eslint-disable no-unused-vars */
import UrlParser from '../../routes/url-parser';
import MakanDbSource from '../../data/makan-db-source';
import {createMakanDetailTemplate} from '../templates/template-creator';
import LikeButtonInitiator from '../../utils/like-button-initiator';

const Detail = {
  async render() {
    return `
        <article id="kedai_list_detail" class="kedais-detail"></article>
        <div id="likeButtonContainer"></div>
      `;
  },

  async afterRender() {
    // menetapkan url parser yang akan digunakan (sumber file dari api-endpoint.js)
    const url = UrlParser.parseActiveUrlWithoutCombiner();
    // menetapkan fungsi dan sumber data fungsi yang akan digunakan (sumber file dari api-endpoint.js)
    const kedaiListDetail = await MakanDbSource.detail(url.id);

    // Menetapkan Kontainer dan css data yang akan digunakan
    const kedaiDetailContainer = document.querySelector('#kedai_list_detail');
    kedaiDetailContainer.innerHTML = createMakanDetailTemplate(kedaiListDetail);

    $(function() {
      $('.stars').stars();
    });

    LikeButtonInitiator.init({
      likeButtonContainer: document.querySelector('#likeButtonContainer'),
      kedaiListDetail: {
        id: kedaiListDetail.restaurant.id,
        city: kedaiListDetail.restaurant.city,
        name: kedaiListDetail.restaurant.name,
        description: kedaiListDetail.restaurant.description,
        pictureId: kedaiListDetail.restaurant.pictureId,
        rating: kedaiListDetail.restaurant.rating,
      },
    });
  },
};

export default Detail;
