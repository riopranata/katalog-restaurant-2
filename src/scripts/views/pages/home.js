/* eslint-disable max-len */
import MakanDbSource from '../../data/makan-db-source';
import {createMakanItemTemplate, createJumbotronHero} from '../templates/template-creator';

const Home = {
  async render() {
    return `
      <div class="jumbotron" id="jumbotron">
       
      </div>

      <div class="content">
          <h2 class="content__heading">Check This Kedai</h2>
          <div id="kedaiList" class="kedais">
          </div>
      </div>
      `;
  },

  async afterRender() {
    // Menetapkan pemanggilan sumber data dari config.js
    const kedaiList = await MakanDbSource.home();

    // Menetapkan kontainer dan css untuk menampilkan jumbotron/hero/header
    const jumbotronContainer = document.querySelector('#jumbotron');
    // Menampilkan isi data dari createJumbotronHero yang di set dari file template-creater
    jumbotronContainer.innerHTML += createJumbotronHero;

    // Menetapkan Kontainer dan css data yang akan digunakan
    const kedaiListContainer = document.querySelector('#kedaiList');

    $(function() {
      $('.stars').stars();
    });

    // Menampilkan isi data dari createMakanItemTemplate yang di set dari file template-creater
    kedaiList.forEach((kedaiItem) => {
      kedaiListContainer.innerHTML += createMakanItemTemplate(kedaiItem);
    });
  },
};

export default Home;
