/* eslint-disable max-len */
import MakanDb from '../../data/makan-db';
import {createMakanItemTemplate, favoriteInfo} from '../templates/template-creator';

const Favorite = {
  async render() {
    return `
    <div class="content">
      <div id="kedais" class="kedais">
      </div>
  </div>`;
  },

  async afterRender() {
    const kedais = await MakanDb.getAllMakans();
    const kedaisContainer = document.querySelector('#kedais');

    $(function() {
      $('.stars').stars();
    });

    if (kedais.length > 0) {
      kedais.map((kedai) => {
        console.log(kedai.name);
        kedaisContainer.innerHTML += createMakanItemTemplate(kedai);
      });
    } else {
      kedaisContainer.innerHTML += favoriteInfo;
    }
  },
};

export default Favorite;
