/* eslint-disable require-jsdoc */
class headerBody extends HTMLElement {
  connectedCallback() {
    this.render();
  }

  render() {
    this.innerHTML = `
      <header class="app-bar">
      <div class="app-bar__menu">
        <button id="hamburgerButton">☰</button>
      </div>
      <div class="app-bar__brand">
        <a>KEDAI KONGKOW</a>
      </div>
      <nav id="navigationDrawer" class="app-bar__navigation">
        <ul>
          <li><a href="#/home">Home</a></li>
          <li><a href="https://www.restiakreasiteknologi.com">About</a></li>
          <li><a href="#/favorite">Favorite</a></li>
        </ul>
      </nav>
    </header>
          `;
  }
}

customElements.define('header-body', headerBody);
