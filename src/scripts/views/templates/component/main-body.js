/* eslint-disable require-jsdoc */
// import '../../../utils/rating-star';

class mainBody extends HTMLElement {
  connectedCallback() {
    this.render();
  }

  render() {
    this.innerHTML = `
      <main id="maincontent"></main>
          `;
  }
}

customElements.define('main-body', mainBody);
