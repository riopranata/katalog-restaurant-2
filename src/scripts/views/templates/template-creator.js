/* eslint-disable max-len */
import CONFIG from '../../globals/config';
import '../../utils/rating-star';

const createJumbotronHero = `
  <div class="hero">
      <img src="./heros/hero-image_2.webp" alt="heroImage">
      <div class="title">
        <h4>KEDAI KONGKOW</h4>
        <p>Rekomendasi Terbaik dari Kami</p>
      </div>
  </div>
    `;

const createMakanDetailTemplate = (kedaiListDetail) => `

  <h2 class="kedai__title">${kedaiListDetail.restaurant.name}</h2>


  <img class="kedai__poster" src="${CONFIG.BASE_IMAGE_URL + kedaiListDetail.restaurant.pictureId}" alt="${kedaiListDetail.restaurant.name}" />

<div class="kedai__info">
  <h3>Information</h3>
  <table>
    <tr>
      <th>Alamat</th>
      <th>:</th>
      <td>${kedaiListDetail.restaurant.address}, ${kedaiListDetail.restaurant.city}</td>
    </tr>
    <tr>
      <th>Overview</th>
      <th>:</th>
      <td>${kedaiListDetail.restaurant.description}</td>
    </tr>
  </table>
  <div class="category">
    <div class="category-menu">
      <table>
        <tr>
          <th>Rating</th>
          <th>:</th>
          <td>${kedaiListDetail.restaurant.rating}  <span style="color:yellow" class="stars kedai-item__header__rating__score" data-rating="${kedaiListDetail.restaurant.rating}" data-num-stars="5"></span></td>
        </tr>
        <tr>
        <tr>
          <th>Kategori Menu</th>
          <th>:</th>
          <td>${kedaiListDetail.restaurant.categories.map((categori) => `<ul><li class="category-name">${categori.name}</li></ul>`).join('')}</td>
        </tr>
        <tr>
          <th>Makanan</th>
          <th>:</th>
          <td>${kedaiListDetail.restaurant.menus.foods.map((food) => `<ul><li class="category-name">${food.name}</li></ul>`).join('')}</td>
        </tr>
        <tr>
          <th>Minuman</th>
          <th>:</th>
          <td>${kedaiListDetail.restaurant.menus.drinks.map((drink) => `<ul><li class="category-name">${drink.name}</li></ul>`).join('')}</td>
        </tr>
      </table>
      <h3>Customer Review</h3>
      <div class="review">${kedaiListDetail.restaurant.customerReviews.map((review) => `
        <div class="review-card">
          <i class="fas fa-user-circle"></i>
          <p class="review-name">${review.name}</p>
          <p class="review-comment">${review.review}</p>
          <p class="review-date">${review.date}</p>
        </div>
        `).join('')}
      </div>
    </div>
`;

const createMakanItemTemplate = (kedaiItem) => `
<div class="kedai-item">
    <div class="kedai-item__header">
        <img 
        class="kedai-item__header__poster" 
        alt="${kedaiItem.name}" 
        src="${kedaiItem.pictureId ?CONFIG.BASE_IMAGE_URL + kedaiItem.pictureId :'https://picsum.photos/id/666/800/450?grayscale'}">
        <div class="kedai-item__header__rating">
            <p>${kedaiItem.city}</p>
            <span class="stars kedai-item__header__rating__score" data-rating="${kedaiItem.rating}" data-num-stars="5"></span>
        </div>
    </div>
    <div class="kedai-item__content">
        <h3>
            <a href="${`/#/detail/${kedaiItem.id}`}">${kedaiItem.name}</a>
        </h3>
        <p>${kedaiItem.description}</p>
        <span><button aria-label="Booking ${kedaiItem.name}">Booking</button></span>
    </div>
</div>
  `;

const createLikeButtonTemplate = () => `
  <button aria-label="like this kedai" id="likeButton" class="like">
     <i class="fa fa-heart-o" aria-hidden="true"></i>
  </button>
`;

const createLikedButtonTemplate = () => `
  <button aria-label="unlike this kedai" id="likeButton" class="like">
    <i class="fa fa-heart" aria-hidden="true"></i>
  </button>
`;

const aboutUsInfo = `
  <div class="aboutus">
      <img src="./etc/under_development.png" alt="Dalam Pengembangan">
      <div>
        <h4>Masih dalam Pengembangan</h4>
        <p>Menunggu Hasil Review dari para Reviewer Dicoding</p>
      </div>
  </div>
    `;

const favoriteInfo = `
<div class="empty-favorite-tag">
  <p>
  <i class="fas fa-heart-broken"></i>
  Maaf, belum ada Kedai Favorit yang bisa disajikan, ayo jadi yang pertama, 
  </p>
</div>
`;

export {
  createMakanItemTemplate,
  createMakanDetailTemplate,
  createJumbotronHero,
  createLikeButtonTemplate,
  createLikedButtonTemplate,
  aboutUsInfo,
  favoriteInfo,
};
