/* eslint-disable max-len */
import MakanDb from '../data/makan-db';
import {createLikeButtonTemplate, createLikedButtonTemplate} from '../views/templates/template-creator';

const LikeButtonInitiator = {
  async init({likeButtonContainer, kedaiListDetail}) {
    this._likeButtonContainer = likeButtonContainer;
    this._kedai = kedaiListDetail;

    await this._renderButton();
  },

  async _renderButton() {
    const {id} = this._kedai;

    if (await this._isKedaiExist(id)) {
      this._renderLiked();
    } else {
      this._renderLike();
    }
  },

  async _isKedaiExist(id) {
    const kedaiListDetail = await MakanDb.getMakan(id);
    return !!kedaiListDetail;
  },

  _renderLike() {
    this._likeButtonContainer.innerHTML = createLikeButtonTemplate();

    const likeButton = document.querySelector('#likeButton');
    likeButton.addEventListener('click', async () => {
      await MakanDb.putMakan(this._kedai);
      this._renderButton();
    });
  },

  _renderLiked() {
    this._likeButtonContainer.innerHTML = createLikedButtonTemplate();

    const likeButton = document.querySelector('#likeButton');
    likeButton.addEventListener('click', async () => {
      await MakanDb.deleteMakan(this._kedai.id);
      this._renderButton();
    });
  },
};

export default LikeButtonInitiator;
